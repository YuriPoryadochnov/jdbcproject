package com.epam.jdbcproject.oracledao;

import com.epam.jdbcproject.daointerfaces.AuthorsDao;
import com.epam.jdbcproject.models.Author;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OracleAuthorsDao implements AuthorsDao {
    private static Logger log = LogManager.getLogger("AuthorsDao");
    private Connection connection;

    public OracleAuthorsDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insertAuthor(Author author) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Authors (AuthorName, DateOfBirth) VALUES (?, ?)");
            preparedStatement.setString(1, author.getName());
            preparedStatement.setString(2, author.getDateOfBirth());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            if (1 == ex.getErrorCode()) {
                log.info("Такой автор уже существует в базе данных.");
            } else {
                ex.printStackTrace();
                log.error("Вставка автора не удалась. Код ошибки: " + ex.getErrorCode());
            }
            return false;
        }
        return true;
    }

    @Override
    public Author selectAuthorByNameAndDateOfBirth(Author author) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT * FROM Authors" +
                            " WHERE AuthorName=? AND DateOfBirth=to_date(?) AND IsHidden='N'");
            preparedStatement.setString(1, author.getName());
            preparedStatement.setString(2, author.getDateOfBirth());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return extractAuthorFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error("Выбрать автора не удалось. Код ошибки: " + e.getErrorCode());
            return null;
        }
        return null;
    }

    @Override
    public boolean deleteAuthorByNameAndDateOfBirth(Author author) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Authors SET IsHidden='Y' WHERE authorName=? AND dateOfBirth=?");
            preparedStatement.setString(1, author.getName());
            preparedStatement.setString(2, author.getDateOfBirth());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException e) {
            log.error("Удаление совершить не удалось. Код ошибки: " + e.getErrorCode());
        }
        return false;
    }

    @Override
    public ArrayList<Author> selectAllAuthors() {
        ArrayList<Author> listOfAuthors = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT authorId, authorName, dateOfBirth FROM Authors WHERE IsHidden='N'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfAuthors.add(extractAuthorFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            log.error("Выбрать автора не удалось. Код ошибки: " + e.getErrorCode());
            return listOfAuthors;
        }
        return listOfAuthors;
    }

    private Author extractAuthorFromResultSet(ResultSet resultSet) throws SQLException {
        Author author = new Author();
        author.setAuthorId(resultSet.getInt("AuthorId"));
        author.setName(resultSet.getString("AuthorName"));
        author.setDateOfBirth(dateParser(resultSet.getString("DateOfBirth")));
        return author;
    }

    private String dateParser(String date) {
        String[] dateWithoutTime = date.split(" ");
        String[] splitDate = dateWithoutTime[0].split("-");
        return splitDate[2] + "." + splitDate[1] + "." + splitDate[0];
    }

    @Override
    public ArrayList<Author> selectAllAuthorsByPartOfAuthorName(String partOfAuthorName) {
        ArrayList<Author> listOfAuthors = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT authorId, authorName, dateOfBirth" +
                            " FROM Authors WHERE IsHidden='N' AND authorName LIKE '%" + partOfAuthorName + "%'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfAuthors.add(extractAuthorFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            log.error("Выбрать автора не удалось. Код ошибки: " + e.getErrorCode());
            return listOfAuthors;
        }
        return listOfAuthors;

    }

    @Override
    public boolean activateAuthor(Author author) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Authors SET isHidden='N' WHERE authorName=? AND DateOfBirth=?");
            preparedStatement.setString(1, author.getName());
            preparedStatement.setString(2, author.getDateOfBirth());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Активировать автора не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }
}

