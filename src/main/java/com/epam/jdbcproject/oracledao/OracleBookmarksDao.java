package com.epam.jdbcproject.oracledao;

import com.epam.jdbcproject.daointerfaces.BookmarksDao;
import com.epam.jdbcproject.models.Bookmark;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OracleBookmarksDao implements BookmarksDao {
    private static Logger log = LogManager.getLogger("ReadByDao");
    private Connection connection;

    public OracleBookmarksDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public ArrayList<Bookmark> selectAllBookmarks() {
        ArrayList<Bookmark> listOfBookmarks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT userName, ISBN, bookMark FROM Bookmarks WHERE IsHidden='N'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBookmarks.add(new Bookmark(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3), false));
            }
        } catch (SQLException e) {
            log.error("Выбрать закладку не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBookmarks;
        }
        return listOfBookmarks;

    }

    @Override
    public boolean insertBookmark(Bookmark bookmark) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Bookmarks (userName, isbn, bookmark) VALUES (?, ?, ?)");
            preparedStatement.setString(1, bookmark.getUserName());
            preparedStatement.setString(2, bookmark.getIsbn());
            preparedStatement.setInt(3, bookmark.getBookmark());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            if (1 == ex.getErrorCode()) {
                log.info("Такой автор уже существует в базе данных.");
            } else {
                log.error("Вставка автора не удалась. Код ошибки: " + ex.getErrorCode());
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteBookmark(Bookmark bookmark) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Bookmarks SET IsHidden='Y' " +
                            "WHERE userName=? AND isbn=? AND isHidden='N'");
            preparedStatement.setString(1, bookmark.getUserName());
            preparedStatement.setString(2, bookmark.getIsbn());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            log.error("Удаление совершить не удалось. Код ошибки: " + e.getErrorCode());
            return false;
        }
        return true;
    }

    @Override
    public boolean updateBookmark(Bookmark bookmark) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Bookmarks SET bookmark=?, isHidden='N' WHERE userName=? AND isbn=?");
            preparedStatement.setInt(1, bookmark.getBookmark());
            preparedStatement.setString(2, bookmark.getUserName());
            preparedStatement.setString(3, bookmark.getIsbn());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Обновление закладки не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }

    @Override
    public Bookmark selectBookmark(Bookmark bookmark) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Bookmarks" +
                    " WHERE userName=? AND isbn=?");
            preparedStatement.setString(1, bookmark.getUserName());
            preparedStatement.setString(2, bookmark.getIsbn());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Bookmark(resultSet.getString(1), resultSet.getString(2),
                        resultSet.getInt(3), "Y".equals(resultSet.getString(4)));
            }
        } catch (SQLException e) {
            log.error("Выбрать пользователя не удалось. Код ошибки: " + e.getErrorCode());
            return null;
        }
        return null;
    }

    @Override
    public ArrayList<Bookmark> selectBookmarkByUserName(String userName) {
        ArrayList<Bookmark> listOfBookmarks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT userName, ISBN, bookMark FROM Bookmarks" +
                            " WHERE IsHidden='N' AND userName=?");
            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBookmarks.add(new Bookmark(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3), false));
            }
        } catch (SQLException e) {
            log.error("Выбрать закладку не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBookmarks;
        }
        return listOfBookmarks;
    }
}

