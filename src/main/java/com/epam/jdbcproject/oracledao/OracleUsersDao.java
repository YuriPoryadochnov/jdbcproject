package com.epam.jdbcproject.oracledao;

import com.epam.jdbcproject.daointerfaces.UsersDao;
import com.epam.jdbcproject.models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OracleUsersDao implements UsersDao {
    private static Logger log = LogManager.getLogger("UsersDao");
    private Connection connection;

    public OracleUsersDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User getUserByUserNameAndPassword(String userName, String userPassword) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT userName, userPassword, isBlocked, Role " +
                            "FROM Users WHERE userName=? AND userPassword=? AND isHidden='N'");
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, userPassword);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return extractUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            log.error("Выбрать пользователя не удалось. Код ошибки: " + e.getErrorCode());
            return null;
        }
        return null;
    }

    @Override
    public ArrayList<User> selectAllUsers() {
        ArrayList<User> listOfUsers = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT userName, userPassword, isBlocked, Role " +
                            "FROM Users WHERE isHidden='N'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfUsers.add(extractUserFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            log.error("Выбрать пользователя не удалось. Код ошибки: " + e.getErrorCode());
            return listOfUsers;
        }
        return listOfUsers;
    }

    @Override
    public boolean insertUser(User user) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO Users (userName, userPassword, role) VALUES (?, ?, ?)");
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getRole());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            if (1 == ex.getErrorCode()) {
                log.info("Такой пользователь уже существует в базе данных.");
            } else {
                log.error("Вставка пользователя не удалась. Код ошибки: " + ex.getErrorCode());
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean activateUser(User user) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Users SET role=?, userPassword=?, isHidden='N' WHERE userName=?");
            preparedStatement.setString(1, user.getRole());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getUserName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Активировать пользователя не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }

    @Override
    public boolean blockUser(String userName) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Users SET isBlocked='Y' WHERE userName=? AND isHidden='N'");
            preparedStatement.setString(1, userName);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Заблокировать пользователя не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }

    @Override
    public boolean unblockUser(String userName) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Users SET isBlocked='N' WHERE userName=? AND isHidden='N'");
            preparedStatement.setString(1, userName);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Разблокировать пользователя не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteUser(String userName) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Users SET isHidden='Y' WHERE userName=? AND isHidden='N'");
            preparedStatement.setString(1, userName);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Удалить пользователя не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }

    private User extractUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setUserName(resultSet.getString("userName"));
        user.setPassword(resultSet.getString("userPassword"));
        user.setIsBlocked("Y".equals(resultSet.getString("isBlocked")));
        user.setRole(resultSet.getString("Role"));
        return user;
    }
}
