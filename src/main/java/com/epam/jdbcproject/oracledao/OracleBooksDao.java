package com.epam.jdbcproject.oracledao;


import com.epam.jdbcproject.daointerfaces.BooksDao;
import com.epam.jdbcproject.models.Book;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OracleBooksDao implements BooksDao {
    private static Logger log = LogManager.getLogger("BooksDao");
    private Connection connection;

    public OracleBooksDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public boolean insertBook(Book book) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT" +
                    " INTO Books(Isbn, bookName, releaseYear, pageCount, publisher, authorId)" +
                    " VALUES (?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, book.getIsbn());
            preparedStatement.setString(2, book.getBookName());
            preparedStatement.setInt(3, book.getReleaseYear());
            preparedStatement.setInt(4, book.getPageCount());
            preparedStatement.setString(5, book.getPublisher());
            preparedStatement.setInt(6, book.getAuthorId());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            if (1 == ex.getErrorCode()) {
                log.info("Такая книга уже есть в базе данных");
            } else {
                log.error("Вставка книги не удалась. Код ошибки: " + ex.getErrorCode());
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteBook(String isbn) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Books SET IsHidden='Y'" +
                    " WHERE Isbn=?");
            preparedStatement.setString(1, isbn);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException e) {
            log.error("Удаление книги не удалось. Код ошибки: " + e.getErrorCode());
        }
        return false;
    }

    @Override
    public boolean deleteBookByAuthorId(int authorId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Books SET IsHidden='Y'" +
                    " WHERE authorId=?");
            preparedStatement.setInt(1, authorId);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException e) {
            log.error("Удаление книги не удалось. Код ошибки: " + e.getErrorCode());
        }
        return false;
    }

    @Override
    public ArrayList<Book> selectAllBooks() {
        ArrayList<Book> listOfBooks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT isbn, bookName, releaseYear, pageCount, publisher, authorId" +
                            " FROM Books WHERE IsHidden='N'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBooks.add(new Book(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getInt(4), resultSet.getString(5),
                        resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            log.error("Выбрать книгу не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBooks;
        }
        return listOfBooks;

    }

    @Override
    public ArrayList<Book> selectAllBooksByPartOfBookName(String partOfBookName) {
        ArrayList<Book> listOfBooks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT isbn, bookName, releaseYear, pageCount, publisher, authorId" +
                            " FROM Books WHERE IsHidden='N' AND bookName LIKE '%" + partOfBookName + "%'");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBooks.add(new Book(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getInt(4), resultSet.getString(5),
                        resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            log.error("Выбрать книгу не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBooks;
        }
        return listOfBooks;

    }

    @Override
    public ArrayList<Book> selectBooksByAuthorId(int authorId) {
        ArrayList<Book> listOfBooks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT isbn, bookName, releaseYear, pageCount, publisher, authorId" +
                            " FROM Books WHERE IsHidden='N' AND authorId=?");
            preparedStatement.setInt(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBooks.add(new Book(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getInt(4), resultSet.getString(5),
                        resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            log.error("Выбрать книгу не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBooks;
        }
        return listOfBooks;

    }

    @Override
    public Book selectBookByIsbn(String isbn) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT isbn, bookName, releaseYear, pageCount, publisher, authorId" +
                            " FROM Books WHERE isHidden='N' AND isbn=?");
            preparedStatement.setString(1, isbn);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Book(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getInt(4), resultSet.getString(5),
                        resultSet.getInt(6));
            }
        } catch (SQLException e) {
            log.error("Выбрать книгу не удалось. Код ошибки: " + e.getErrorCode());
            return null;
        }
        return null;
    }

    @Override
    public ArrayList<Book> selectBooksByRangeOfReleaseYears(int lowerLimit, int upperLimit) {
        ArrayList<Book> listOfBooks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT isbn, bookName, releaseYear, pageCount, publisher, authorId" +
                            " FROM Books" +
                            " WHERE isHidden='N' AND releaseYear BETWEEN " + lowerLimit + " AND " + upperLimit);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBooks.add(new Book(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getInt(4), resultSet.getString(5),
                        resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            log.error("Выбрать книгу не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBooks;
        }
        return listOfBooks;
    }


    @Override
    public ArrayList<Book> selectBooksByReleaseYearPageCountPartOfBookName(int releaseYear,
                                                                           int pageCount, String partOfBookName) {
        ArrayList<Book> listOfBooks = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT isbn, bookName, releaseYear, pageCount, publisher, authorId" +
                            " FROM Books" +
                            " WHERE isHidden='N' AND releaseYear=? AND" +
                            " pageCount=? AND bookName LIKE '%" + partOfBookName + "%'");
            preparedStatement.setInt(1, releaseYear);
            preparedStatement.setInt(2, pageCount);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfBooks.add(new Book(resultSet.getString(1),
                        resultSet.getString(2), resultSet.getInt(3),
                        resultSet.getInt(4), resultSet.getString(5),
                        resultSet.getInt(6)));
            }
        } catch (SQLException e) {
            log.error("Выбрать книгу не удалось. Код ошибки: " + e.getErrorCode());
            return listOfBooks;
        }
        return listOfBooks;

    }

    @Override
    public boolean activateBook(Book book) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("UPDATE Books SET bookName=?, releaseYear=?, pageCount=?, publisher=?," +
                            " authorId=?, isHidden='N' WHERE isbn=?");
            preparedStatement.setString(1, book.getBookName());
            preparedStatement.setInt(2, book.getReleaseYear());
            preparedStatement.setInt(3, book.getPageCount());
            preparedStatement.setString(4, book.getPublisher());
            preparedStatement.setInt(5, book.getAuthorId());
            preparedStatement.setString(6, book.getIsbn());
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Активировать книгу не удалось. Код ошибки: " + ex.getErrorCode());
            return false;
        }
        return true;
    }
}
