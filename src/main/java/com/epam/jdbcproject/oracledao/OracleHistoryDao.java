package com.epam.jdbcproject.oracledao;

import com.epam.jdbcproject.daointerfaces.HistoryDao;
import com.epam.jdbcproject.models.Action;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OracleHistoryDao implements HistoryDao {
    private static Logger log = LogManager.getLogger("HistoryDao");
    private Connection connection;

    public OracleHistoryDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void insertAction(String userName, String actionMessage) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("INSERT INTO History (userName, action) VALUES (?, ?)");
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, actionMessage);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            log.error("Вставка действия не удалась. Код ошибки: " + ex.getErrorCode());
        }
    }

    @Override
    public ArrayList<Action> selectAllActions(String userName) {
        ArrayList<Action> listOfActions = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("SELECT actionId, userName, action FROM History " +
                            "WHERE userName=? AND IsHidden='N' ORDER BY actionId ASC");
            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                listOfActions.add(new Action(resultSet.getInt(1), resultSet.getString(2),
                        resultSet.getString(3)));
            }
        } catch (SQLException e) {
            log.error("Выбрать действие не удалось. Код ошибки: " + e.getErrorCode());
            return listOfActions;
        }
        return listOfActions;
    }
}
