package com.epam.jdbcproject.library;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.models.User;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class EntranceLibrary {
    private static Logger log = LogManager.getLogger("EntranceLibrary");

    private Dao dao;
    private Scanner scanner;

    public EntranceLibrary(Dao dao, Scanner scanner) {
        this.dao = dao;
        this.scanner = scanner;
    }

    public User enterLibrary() {
        while (true) {
            System.out.println("Введите имя пользователя: ");
            String name = scanner.nextLine();
            System.out.println("Введите пароль: ");
            String password = scanner.nextLine();
            User user = dao.getUsersDao().getUserByUserNameAndPassword(name, password);
            if (user.isBlocked()) {
                System.out.println("Пользователь заблокирован");
                continue;
            }
            if (user == null) {
                System.out.println("Неправильные имя пользователя или пароль");
            } else {
                dao.getHistoryDao().insertAction(user.getUserName(), "Пользователь вошёл в систему");
                System.out.println("Вы вошли в систему");
                return user;
            }
        }
    }
}
