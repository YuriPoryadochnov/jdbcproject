package com.epam.jdbcproject.library;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.daofactory.OracleDao;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class OracleLibrary {
    private Dao dao;
    private UserMenu userMenu;

    public OracleLibrary() {
        dao = new OracleDao();
        userMenu = new UserMenu(dao);
    }

    public void launch() throws IOException, ParseException {
        dao.createConnection();
        userMenu.selectCommand();
    }
}
