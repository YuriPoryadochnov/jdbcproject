package com.epam.jdbcproject.library;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;

public class JdbcApplication {
    public static void main(String[] args) throws SQLException, IOException, ParseException {
        OracleLibrary library = new OracleLibrary();
        library.launch();
    }
}
