package com.epam.jdbcproject.library;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.models.User;
import com.epam.jdbcproject.operations.DeleteOperations;
import com.epam.jdbcproject.operations.InsertOperations;
import com.epam.jdbcproject.operations.SelectOperations;
import com.epam.jdbcproject.operations.UpdateOperations;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Scanner;

public class UserMenu {
    private User user;
    private Scanner scanner;
    private EntranceLibrary entranceLibrary;
    private InsertOperations insertOperations;
    private DeleteOperations deleteOperations;
    private SelectOperations selectOperations;
    private UpdateOperations updateOperations;

    public UserMenu(Dao dao) {
        scanner = new Scanner(System.in);
        entranceLibrary = new EntranceLibrary(dao, scanner);
        insertOperations = new InsertOperations(dao, scanner);
        deleteOperations = new DeleteOperations(dao, scanner);
        selectOperations = new SelectOperations(dao, scanner);
        updateOperations = new UpdateOperations(dao, scanner);
    }

    public void selectCommand() throws IOException, ParseException {
        setUser();
        String userInput;
        while (true) {
            System.out.println("Пользовательские команды:\n1. сменить пользователя\n2. добавить книгу\n" +
                    "3. удалить книгу\n4. добавить автора\n5. удалить автора\n6. добавить книги из json\n" +
                    "7. добавить книги из csv\n8. добавить закладку\n9. удалить закладку\n" +
                    "10. выбрать книги по части названия\n11. выбрать книги по части имени автора\n" +
                    "12. выбрать книгу по isbn\n13. выбрать книги по диапозону годов\n" +
                    "14. выбрать книги по дате выпуска, количеству страниц и части названия\n" +
                    "15. выбрать книги, в которых есть закладки пользователя\n16. вывести всех авторов\n" +
                    "17. вывести все книги\n18. вывести все закладки\n19. перейти к командам администратора\n" +
                    "20. выйти");
            userInput = scanner.nextLine();
            switch (userInput) {
                case "1":
                    setUser();
                    break;
                case "2":
                    insertOperations.insertBook();
                    break;
                case "3":
                    deleteOperations.deleteBook();
                    break;
                case "4":
                    insertOperations.insertAuthor();
                    break;
                case "5":
                    deleteOperations.deleteAuthor();
                    break;
                case "6":
                    insertOperations.insertBooksFromJson();
                    break;
                case "7":
                    insertOperations.insertBooksFromCsv();
                    break;
                case "8":
                    insertOperations.insertBookmark();
                    break;
                case "9":
                    deleteOperations.deleteBookmark();
                    break;
                case "10":
                    selectOperations.selectAllBooksByPartOfBookName();
                    break;
                case "11":
                    selectOperations.selectAllBooksByPartOfAuthorName();
                    break;
                case "12":
                    selectOperations.selectBooksByIsbn();
                    break;
                case "13":
                    selectOperations.selectBooksByRangeOfReleaseYears();
                    break;
                case "14":
                    selectOperations.selectBooksByReleaseYearPageCountPartOfBookName();
                    break;
                case "15":
                    selectOperations.selectBooksByBookmarks();
                    break;
                case "16":
                    selectOperations.selectAllAuthors();
                    break;
                case "17":
                    selectOperations.selectAllBooks();
                    break;
                case "18":
                    selectOperations.selectAllBookmarks();
                    break;
                case "19":
                    selectAdminCommand();
                    break;
                case "20":
                    Runtime.getRuntime().exit(0);
            }
        }
    }

    private void selectAdminCommand() {
        if ("User".equals(user.getRole())) {
            System.out.println("Вы не являетесь администратором");
            return;
        }
        String adminInput;
        while (true) {
            System.out.println("1.добавить нового пользователя\n2.заблокировать пользователя\n" +
                    "3.разблокировать пользователя\n4.вывести историю всех действий пользователя\n" +
                    "5.выйти из режима администратора");
            adminInput = scanner.nextLine();
            switch (adminInput) {
                case "1":
                    insertOperations.insertUser();
                    break;
                case "2":
                    updateOperations.blockUser();
                    break;
                case "3":
                    updateOperations.unblockUser();
                    break;
                case "4":
                    selectOperations.selectAllActions();
                    break;
                case "5":
                    return;
            }
        }
    }

    private void setUser() {
        user = entranceLibrary.enterLibrary();
        insertOperations.setUser(user);
        deleteOperations.setUser(user);
        selectOperations.setUser(user);
        updateOperations.setUser(user);
    }
}
