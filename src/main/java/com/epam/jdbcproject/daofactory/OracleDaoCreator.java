package com.epam.jdbcproject.daofactory;

public class OracleDaoCreator extends DaoCreator {

    @Override
    public Dao createDao() {
        return new OracleDao();
    }
}
