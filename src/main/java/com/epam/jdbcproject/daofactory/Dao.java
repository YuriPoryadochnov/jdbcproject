package com.epam.jdbcproject.daofactory;

import com.epam.jdbcproject.daointerfaces.*;

import java.sql.Connection;

public interface Dao {

    void createConnection();

    Connection getConnection();

    UsersDao getUsersDao();

    BookmarksDao getBookmarksDao();

    BooksDao getBooksDao();

    AuthorsDao getAuthorsDao();

    HistoryDao getHistoryDao();
}
