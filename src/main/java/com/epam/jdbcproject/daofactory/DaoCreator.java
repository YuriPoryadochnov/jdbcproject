package com.epam.jdbcproject.daofactory;

abstract class DaoCreator {
    public abstract Dao createDao();
}
