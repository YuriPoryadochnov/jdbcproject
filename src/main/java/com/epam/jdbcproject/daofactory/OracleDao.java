package com.epam.jdbcproject.daofactory;

import com.epam.jdbcproject.daointerfaces.*;
import com.epam.jdbcproject.oracledao.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class OracleDao implements Dao {
    private String driver;
    private String url;
    private String user;
    private String password;
    private UsersDao usersDao;
    private BookmarksDao bookmarksDao;
    private BooksDao booksDao;
    private AuthorsDao authorsDao;
    private HistoryDao historyDao;
    private Connection connection;

    @Override
    public void createConnection() {
        getInformationFromPropertyFile();
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            System.out.println("Проблема с драйвером");
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getInformationFromPropertyFile() {
        Properties property = new Properties();
        try {
            FileInputStream configurationFile = new FileInputStream("src/main/resources/db.properties");
            property.load(configurationFile);
            driver = property.getProperty("oracleDriver");
            url = property.getProperty("oracleUrl");
            user = property.getProperty("oracleUser");
            password = property.getProperty("oraclePassword");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() {
        if (connection == null) {
            createConnection();
        }
        return connection;
    }

    @Override
    public UsersDao getUsersDao() {
        if (usersDao == null) {
            usersDao = new OracleUsersDao(connection);
        }
        return usersDao;
    }

    @Override
    public BookmarksDao getBookmarksDao() {
        if (bookmarksDao == null) {
            bookmarksDao = new OracleBookmarksDao(connection);
        }
        return bookmarksDao;
    }

    @Override
    public BooksDao getBooksDao() {
        if (booksDao == null) {
            booksDao = new OracleBooksDao(connection);
        }
        return booksDao;
    }

    @Override
    public AuthorsDao getAuthorsDao() {
        if (authorsDao == null) {
            authorsDao = new OracleAuthorsDao(connection);
        }
        return authorsDao;
    }

    @Override
    public HistoryDao getHistoryDao() {
        if (historyDao == null) {
            historyDao = new OracleHistoryDao(connection);
        }
        return historyDao;
    }
}
