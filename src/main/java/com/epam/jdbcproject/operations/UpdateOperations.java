package com.epam.jdbcproject.operations;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.models.User;

import java.util.Scanner;

public class UpdateOperations {
    private Dao dao;
    private Scanner scanner;
    private User user;

    public UpdateOperations(Dao dao, Scanner scanner) {
        this.dao = dao;
        this.scanner = scanner;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void blockUser() {
        System.out.println("Введите имя пользователя которого хотите заблокировать: ");
        String userName = scanner.nextLine();
        implementBlockUser(userName);
    }

    public void implementBlockUser(String userName) {
        if (dao.getUsersDao().blockUser(userName)) {
            dao.getHistoryDao().insertAction(user.getUserName(), "Заблокировал пользователя " + userName);
        }
    }

    public void unblockUser() {
        System.out.println("Введите имя пользователя которого хотите разблокировать: ");
        String userName = scanner.nextLine();
        implementUnblockUser(userName);
    }

    public void implementUnblockUser(String userName) {
        if (dao.getUsersDao().unblockUser(userName)) {
            dao.getHistoryDao().insertAction(user.getUserName(),
                    "Разблокировал пользователя " + userName);
        }
    }
}
