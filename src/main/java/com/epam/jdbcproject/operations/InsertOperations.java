package com.epam.jdbcproject.operations;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.models.Author;
import com.epam.jdbcproject.models.Book;
import com.epam.jdbcproject.models.Bookmark;
import com.epam.jdbcproject.models.User;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class InsertOperations {
    private Dao dao;
    private Scanner scanner;
    private User user;
    private static Logger log;

    public InsertOperations(Dao dao, Scanner scanner) {
        this.dao = dao;
        this.scanner = scanner;
        log = LogManager.getLogger("InsertOperation");
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void insertBook() {
        Book book = new Book();
        System.out.println("Введите ISBN:");
        book.setIsbn(scanner.nextLine());
        System.out.println("Введите название книги:");
        book.setBookName(scanner.nextLine());
        System.out.println("Введите год издания:");
        book.setReleaseYear(Integer.parseInt(scanner.nextLine()));
        System.out.println("Введите количество страниц:");
        book.setPageCount(Integer.parseInt(scanner.nextLine()));
        System.out.println("Введите издателя:");
        book.setPublisher(scanner.nextLine());
        book.setAuthorId(insertAuthor().getAuthorId());
        implementInsertBook(book);
    }

    public Author insertAuthor() {
        Author author = new Author();
        System.out.println("Введите имя автора:");
        author.setName(scanner.nextLine());
        System.out.println("Введите дату рождения автора:");
        author.setDateOfBirth(scanner.nextLine());
        return implementInsertAuthor(author);
    }

    public void insertBooksFromJson() throws IOException, ParseException {
        System.out.println("Введите путь к файлу json: ");
        FileReader reader = new FileReader(scanner.nextLine());
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = (JSONArray) jsonParser.parse(reader);
        Iterator iterator = jsonArray.iterator();
        JSONObject jsonObject;
        Book book = new Book();
        Author author = new Author();
        while (iterator.hasNext()) {
            jsonObject = (JSONObject) iterator.next();
            JSONObject jsonAuthor = (JSONObject) jsonParser.parse(jsonObject.get("author").toString());
            if ("".equals(jsonAuthor.get("secondName").toString())) {
                author.setName(jsonAuthor.get("lastName").toString() + " " + jsonAuthor.get("name").toString());
            } else {
                author.setName(jsonAuthor.get("lastName").toString() + " " + jsonAuthor.get("name").toString() +
                        " " + jsonAuthor.get("secondName").toString());
            }
            author.setDateOfBirth(jsonAuthor.get("dob").toString());
            book.setIsbn(jsonObject.get("ISBN").toString());
            book.setBookName(jsonObject.get("bookName").toString());
            book.setReleaseYear(Integer.parseInt(jsonObject.get("releaseYear").toString()));
            book.setPageCount(Integer.parseInt(jsonObject.get("pageCount").toString()));
            book.setPublisher(jsonObject.get("publisher").toString());

            book.setAuthorId(implementInsertAuthor(author).getAuthorId());
            implementInsertBook(book);
        }
    }

    public void insertBooksFromCsv() {
        try {
            System.out.println("Введите путь к файлу csv: ");
            FileReader reader = new FileReader(scanner.nextLine());
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
            List<String[]> csvData = csvReader.readAll();
            Author author = new Author();
            Book book = new Book();
            for (String[] row :
                    csvData) {
                book.setBookName(row[0]);
                book.setReleaseYear(Integer.parseInt(row[1]));
                book.setPageCount(Integer.parseInt(row[2]));
                book.setIsbn(row[3]);
                book.setPublisher(row[4]);
                if ("".equals(row[7])) {
                    author.setName(row[5] + " " + row[6]);
                } else {
                    author.setName(row[5] + " " + row[6] + " " + row[7]);
                }
                author.setDateOfBirth(row[8]);
                book.setAuthorId(implementInsertAuthor(author).getAuthorId());
                implementInsertBook(book);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл по указанному пути не найден");
        } catch (IOException e) {
            log.error("Ошибка! " + e.getMessage());
        }
    }

    public Author implementInsertAuthor(Author author) {
        if (dao.getAuthorsDao().insertAuthor(author)) {
            author = dao.getAuthorsDao().selectAuthorByNameAndDateOfBirth(author);
            dao.getHistoryDao().insertAction(user.getUserName(), "Пользователь " + user.getUserName() +
                    " добавил автора " + author.getAuthorId());
            return author;
        } else if (dao.getAuthorsDao().activateAuthor(author)) {
            author = dao.getAuthorsDao().selectAuthorByNameAndDateOfBirth(author);
            dao.getHistoryDao().insertAction(user.getUserName(), "Пользователь " + user.getUserName() +
                    " добавил автора " + author.getAuthorId());
            return author;
        }
        return null;
    }

    public void implementInsertBook(Book book) {
        if (dao.getBooksDao().insertBook(book)) {
            dao.getHistoryDao().insertAction(user.getUserName(), "Пользователь добавил книгу " +
                    book.getIsbn());
        } else if (dao.getBooksDao().activateBook(book)) {
            dao.getHistoryDao().insertAction(user.getUserName(), "Пользователь добавил книгу " +
                    book.getIsbn());
        }
    }

    public void insertBookmark() {
        Bookmark bookmark = new Bookmark();
        bookmark.setUserName(user.getUserName());
        System.out.println("Введите isbn книги, в которую хотите добавить закладку: ");
        bookmark.setIsbn(scanner.nextLine());
        Book book = dao.getBooksDao().selectBookByIsbn(bookmark.getIsbn());
        if (book == null) {
            System.out.println("Такой книги нет в базе данных");
            return;
        }
        System.out.println("Введите номер страницы: ");
        bookmark.setBookmark(Integer.parseInt(scanner.nextLine()));
        if (book.getPageCount() < bookmark.getBookmark() || bookmark.getBookmark() <= 0) {
            System.out.println("Неправильный ввод количества страниц или такой страницы в книге не существует");
            return;
        }
        implementInsertBookmark(bookmark);
    }

    public void implementInsertBookmark(Bookmark bookmark) {
        if (dao.getBookmarksDao().insertBookmark(bookmark)) {
            dao.getHistoryDao().insertAction(bookmark.getUserName(),
                    "Пользоваватель добавил новую закладку в книгу " + bookmark.getIsbn());
        } else if (dao.getBookmarksDao().updateBookmark(bookmark)) {
            dao.getHistoryDao().insertAction(bookmark.getUserName(),
                    "Пользоваватель добавил новую закладку в книгу " + bookmark.getIsbn());
        }
    }

    public void insertUser() {
        User newUser = new User();
        System.out.println("Введите имя пользователя: ");
        newUser.setUserName(scanner.nextLine());
        System.out.println("Введите пароль: ");
        newUser.setPassword(scanner.nextLine());
        System.out.println("Введите роль пользователя в системе (User/Admin): ");
        newUser.setRole(scanner.nextLine());
        implementInsertUser(newUser);
    }

    public void implementInsertUser(User newUser) {
        if (dao.getUsersDao().insertUser(newUser)) {
            dao.getHistoryDao().insertAction(user.getUserName(),
                    "Пользоваватель добавил нового пользователя " + newUser.getUserName() + " в систему");
        } else if (dao.getUsersDao().activateUser(newUser)) {
            dao.getHistoryDao().insertAction(user.getUserName(),
                    "Пользоваватель добавил нового пользователя " + newUser.getUserName() + " в систему");
        }

    }
}

