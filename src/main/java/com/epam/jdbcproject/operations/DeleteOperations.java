package com.epam.jdbcproject.operations;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.models.Author;
import com.epam.jdbcproject.models.Bookmark;
import com.epam.jdbcproject.models.User;

import java.util.Scanner;

public class DeleteOperations {
    private Dao dao;
    private Scanner scanner;
    private User user;

    public DeleteOperations(Dao dao, Scanner scanner) {
        this.dao = dao;
        this.scanner = scanner;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void deleteBook() {
        System.out.println("Введите ISBN:");
        String input = scanner.nextLine();
        implementDeleteBook(input);
    }

    public void implementDeleteBook(String isbn) {
        if (dao.getBooksDao().deleteBook(isbn)) {
            dao.getHistoryDao().insertAction(user.getUserName(),
                    "Пользователь " + user.getUserName() + " удалил книгу " + isbn);
        }
    }

    public void deleteAuthor() {
        Author author = new Author();
        System.out.println("Введите имя автора:");
        author.setName(scanner.nextLine());
        System.out.println("Введите дату рождения:");
        author.setDateOfBirth(scanner.nextLine());
        implementDeleteAuthor(author);
    }

    public void implementDeleteAuthor(Author author) {
        author = dao.getAuthorsDao().selectAuthorByNameAndDateOfBirth(author);
        if (dao.getAuthorsDao().deleteAuthorByNameAndDateOfBirth(author) &&
                dao.getBooksDao().deleteBookByAuthorId(author.getAuthorId())) {
            dao.getHistoryDao().insertAction(user.getUserName(), "Пользователь " + user.getUserName() +
                    " удалил автора " + author.getAuthorId() + " и все книги, которые с ним связаны");
        }
    }

    public void deleteBookmark() {
        Bookmark bookmark = new Bookmark();
        bookmark.setUserName(user.getUserName());
        System.out.println("Введите isbn:");
        bookmark.setIsbn(scanner.nextLine());
        implementDeleteBookmark(bookmark);
    }

    public void implementDeleteBookmark(Bookmark bookmark) {
        if (dao.getBookmarksDao().selectBookmark(bookmark) == null) {
            System.out.println("Такой закладки нет");
        } else if (dao.getBookmarksDao().deleteBookmark(bookmark)) {
            dao.getHistoryDao().insertAction(bookmark.getUserName(),
                    "Пользователь удалил закладку в книге " + bookmark.getIsbn());
        }
    }

    public void implementDeleteUser(String userName) {
        if (dao.getUsersDao().deleteUser(userName)) {
            dao.getHistoryDao().insertAction(user.getUserName(),
                    "Пользователь " + user.getUserName() + " удалил пользователя " + userName);
        }
    }
}
