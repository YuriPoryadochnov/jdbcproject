package com.epam.jdbcproject.operations;

import com.epam.jdbcproject.daofactory.Dao;
import com.epam.jdbcproject.models.*;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;

public class SelectOperations {
    private Dao dao;
    private Scanner scanner;
    private User user;

    public SelectOperations(Dao dao, Scanner scanner) {
        this.dao = dao;
        this.scanner = scanner;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void selectAllAuthors() {
        ListIterator<Author> listIterator = implementSelectAllAuthors().listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().toString());
        }
    }

    public ArrayList<Author> implementSelectAllAuthors() {
        return dao.getAuthorsDao().selectAllAuthors();
    }

    public void selectAllBooks() {
        ListIterator<Book> listIterator = implementSelectAllBooks().listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().toString());
        }
    }

    public ArrayList<Book> implementSelectAllBooks() {
        return dao.getBooksDao().selectAllBooks();
    }

    public ArrayList<User> implementSelectAllUsers() {
        return dao.getUsersDao().selectAllUsers();
    }

    public void selectAllActions() {
        System.out.println("Введите имя пользователя, историю действий которого вы хотите увидеть: ");
        ListIterator<Action> listIterator = implementSelectAllActions(scanner.nextLine()).listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().toString());
        }
    }

    public ArrayList<Action> implementSelectAllActions(String text) {
        return dao.getHistoryDao().selectAllActions(text);
    }

    public void selectAllBookmarks() {
        ListIterator<Bookmark> listIterator = implementSelectAllBookmarks().listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().toString());
        }
    }

    public ArrayList<Bookmark> implementSelectAllBookmarks() {
        return dao.getBookmarksDao().selectAllBookmarks();
    }

    public void selectAllBooksByPartOfBookName() {
        System.out.println("Введите часть названия книги: ");
        ListIterator<Book> listIterator = implementSelectAllBooksByPartOfBookName(scanner.nextLine()).listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().toString());
        }
    }

    public ArrayList<Book> implementSelectAllBooksByPartOfBookName(String text) {
        return dao.getBooksDao().selectAllBooksByPartOfBookName(text);
    }

    public void selectAllBooksByPartOfAuthorName() {
        System.out.println("Введите часть имени автора: ");
        for (Book book :
                implementSelectAllBooksByPartOfAuthorName(scanner.nextLine())) {
            System.out.println(book.toString());
        }
    }

    public ArrayList<Book> implementSelectAllBooksByPartOfAuthorName(String text) {
        ListIterator<Author> listIterator = selectAllAuthorsByPartOfAuthorName(text).listIterator();
        ArrayList<Book> listOfBooks = new ArrayList<>();
        while (listIterator.hasNext()) {
            listOfBooks.addAll(dao.getBooksDao().selectBooksByAuthorId(listIterator.next().getAuthorId()));
        }
        return listOfBooks;
    }

    public ArrayList<Author> selectAllAuthorsByPartOfAuthorName(String text) {
        return dao.getAuthorsDao().selectAllAuthorsByPartOfAuthorName(text);
    }

    public void selectBooksByIsbn() {
        System.out.println("Введите isbn:");
        Book book = implementSelectBooksByIsbn(scanner.nextLine());
        if (book == null) {
            System.out.println("По указанному isbn книги не найдено");
        } else {
            System.out.println(book.toString());
        }
    }

    public Book implementSelectBooksByIsbn(String text) {
        return dao.getBooksDao().selectBookByIsbn(text);
    }

    public void selectBooksByRangeOfReleaseYears() {
        System.out.println("Введите нижниюю границу: ");
        int lowerLimit = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите верхнюю границу: ");
        for (Book book :
                implementSelectBooksByRangeOfReleaseYears(lowerLimit, Integer.parseInt(scanner.nextLine()))) {
            System.out.println(book.toString());
        }
    }

    public ArrayList<Book> implementSelectBooksByRangeOfReleaseYears(int lowerLimit, int upperLimit) {
        return dao.getBooksDao().selectBooksByRangeOfReleaseYears(lowerLimit, upperLimit);
    }

    public void selectBooksByReleaseYearPageCountPartOfBookName() {
        System.out.println("Введите год выпуска: ");
        int releaseYear = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите количество страниц: ");
        int pageCount = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите часть наименования книги: ");
        for (Book book :
                implementSelectBooksByReleaseYearPageCountPartOfBookName(releaseYear, pageCount, scanner.nextLine())) {
            System.out.println(book.toString());
        }
    }

    public ArrayList<Book> implementSelectBooksByReleaseYearPageCountPartOfBookName(int releaseYear, int pageCount,
                                                                                    String bookName) {
        return dao.getBooksDao().selectBooksByReleaseYearPageCountPartOfBookName(releaseYear, pageCount, bookName);
    }

    public void selectBooksByBookmarks() {
        ListIterator<Book> listIterator = implementSelectBooksByBookmarks().listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next().toString());
        }
    }

    public ArrayList<Book> implementSelectBooksByBookmarks() {
        ListIterator<Bookmark> listIterator = selectBookmarkByUserName(user.getUserName()).listIterator();
        ArrayList<Book> listOfBooks = new ArrayList<>();
        while (listIterator.hasNext()) {
            listOfBooks.add(dao.getBooksDao().selectBookByIsbn(listIterator.next().getIsbn()));
        }
        return listOfBooks;
    }

    public ArrayList<Bookmark> selectBookmarkByUserName(String userName) {
        return dao.getBookmarksDao().selectBookmarkByUserName(userName);
    }
}
