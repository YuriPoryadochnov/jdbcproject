package com.epam.jdbcproject.models;

public class Action {
    private int actionId;
    private String userName;
    private String action;

    public Action() {
    }

    public Action(int actionId, String userName, String action) {
        this.actionId = actionId;
        this.userName = userName;
        this.action = action;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "Action id: " + actionId + " User name: " + userName + " Action: " + action;
    }
}
