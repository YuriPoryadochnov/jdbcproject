package com.epam.jdbcproject.models;

public class Book {
    private String isbn;
    private String bookName;
    private int releaseYear;
    private int pageCount;
    private String publisher;
    private int authorId;

    public Book() {
    }

    public Book(String isbn, String bookName, int releaseYear, int pageCount, String publisher, int authorId) {
        this.isbn = isbn;
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.publisher = publisher;
        this.authorId = authorId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    @Override
    public String toString() {
        return "ISBN: " + isbn + " Book name: " + bookName + " Release year: " + releaseYear + " Page count: " +
                pageCount + " Publisher: " + publisher + " Author id: " + authorId;
    }
}
