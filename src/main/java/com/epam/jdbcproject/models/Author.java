package com.epam.jdbcproject.models;

public class Author {
    private int authorId;
    private String name;
    private String dateOfBirth;

    public Author() {
    }

    public Author(int authorId, String name, String dateOfBirth) {
        this.authorId = authorId;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Author id: " + authorId + " Name: " + name + " Date of birth: " + dateOfBirth;
    }
}
