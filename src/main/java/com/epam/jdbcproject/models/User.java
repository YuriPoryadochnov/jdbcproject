package com.epam.jdbcproject.models;

public class User {
    private String userName;
    private boolean isBlocked = false;
    private String role;
    private String password;

    public User() {
    }

    public User(String userName, String password, boolean isBlocked, String role) {
        this.userName = userName;
        this.password = password;
        this.isBlocked = isBlocked;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public String getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setIsBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
