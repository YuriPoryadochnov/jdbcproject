package com.epam.jdbcproject.models;


public class Bookmark {
    private String userName;
    private String isbn;
    private int bookmark;
    private boolean isHidden;

    public Bookmark() {
    }

    public Bookmark(String userName, String isbn, int bookmark, boolean isHidden) {
        this.userName = userName;
        this.isbn = isbn;
        this.bookmark = bookmark;
        this.isHidden = isHidden;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getBookmark() {
        return bookmark;
    }

    public void setBookmark(int bookmark) {
        this.bookmark = bookmark;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    @Override
    public String toString() {
        return "User name: " + userName + " ISBN: " + isbn + " Bookmark: " + bookmark;
    }
}
