package com.epam.jdbcproject.daointerfaces;

import com.epam.jdbcproject.models.Bookmark;

import java.util.ArrayList;

public interface BookmarksDao {
    ArrayList<Bookmark> selectAllBookmarks();

    boolean insertBookmark(Bookmark bookmark);

    boolean deleteBookmark(Bookmark bookmark);

    boolean updateBookmark(Bookmark bookmark);

    Bookmark selectBookmark(Bookmark bookmark);

    ArrayList<Bookmark> selectBookmarkByUserName(String userName);
}
