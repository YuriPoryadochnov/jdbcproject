package com.epam.jdbcproject.daointerfaces;

import com.epam.jdbcproject.models.Book;

import java.util.ArrayList;

public interface BooksDao {
    boolean insertBook(Book book);

    boolean deleteBook(String isbn);

    boolean deleteBookByAuthorId(int authorId);

    ArrayList<Book> selectAllBooks();

    ArrayList<Book> selectAllBooksByPartOfBookName(String partOfBookName);

    ArrayList<Book> selectBooksByAuthorId(int authorId);

    Book selectBookByIsbn(String isbn);

    ArrayList<Book> selectBooksByRangeOfReleaseYears(int lowerLimit, int upperLimit);

    ArrayList<Book> selectBooksByReleaseYearPageCountPartOfBookName(int releaseYear,
                                                                    int pageCount, String partOfBookName);

    boolean activateBook(Book book);
}
