package com.epam.jdbcproject.daointerfaces;

import com.epam.jdbcproject.models.Action;

import java.util.ArrayList;

public interface HistoryDao {
    void insertAction(String userName, String actionMessage);

    ArrayList<Action> selectAllActions(String userName);
}
