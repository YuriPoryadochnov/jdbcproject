package com.epam.jdbcproject.daointerfaces;

import com.epam.jdbcproject.models.Author;

import java.util.ArrayList;

public interface AuthorsDao {
    boolean insertAuthor(Author author);

    Author selectAuthorByNameAndDateOfBirth(Author author);

    boolean deleteAuthorByNameAndDateOfBirth(Author author);

    ArrayList<Author> selectAllAuthors();

    ArrayList<Author> selectAllAuthorsByPartOfAuthorName(String partOfAuthorName);

    boolean activateAuthor(Author author);
}
