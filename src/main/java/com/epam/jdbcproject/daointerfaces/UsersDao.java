package com.epam.jdbcproject.daointerfaces;

import com.epam.jdbcproject.models.User;

import java.util.ArrayList;

public interface UsersDao {
    User getUserByUserNameAndPassword(String userName, String userPassword);

    ArrayList<User> selectAllUsers();

    boolean insertUser(User user);

    boolean activateUser(User user);

    boolean blockUser(String userName);

    boolean unblockUser(String userName);

    boolean deleteUser(String userName);
}
